import { Component, OnInit } from '@angular/core';
import { User, Usersignup } from 'src/app/core/models/user';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public user: Usersignup = {
    username: '',
    email: '',
    password1: '',
    password2: '',
  };

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
  }

  public signUp() {
    this.userService.signUp(this.user).subscribe(
      (response) => {
        this.router.navigate(["/login"]);
        console.log("signup successfull");
        console.log(response);
      },
      (error) => {
        if((!this.user.email.includes('@')) && (!this.user.email.includes('.com'))){
          alert('worng email address');
        }
        console.log(error);
      }
    );
  }

}
