import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { combineAll } from 'rxjs/operators';
import { ResumelistComponent } from './resumelist/resumelist.component';
import { AddResumeProfileComponent } from './add-resume-profile/add-resume-profile.component';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';


const routes: Routes = [
    {
        path: 'signup',
        component: SignupComponent,
    },
    {
      path: 'login',
      component: LoginComponent,
    },
    {
      path: 'resumelist',
      component: ResumelistComponent,
    },
    {
      path: 'addprofile',
      component: AddResumeProfileComponent,
    },
    {
      path: 'profile/:id',
      component: ProfileDetailsComponent,
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
