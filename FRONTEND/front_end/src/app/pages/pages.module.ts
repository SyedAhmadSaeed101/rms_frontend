import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup/signup.component';
import { PagesRoutingModule } from './pages-routing.module';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
//import { HomeComponent } from './home/home.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ChartsModule } from 'ng2-charts'
// import { SearchComponent } from './search/search.component';
// import { StorydetailsComponent } from './storydetails/storydetails.component';
// import { ProfileComponent } from './profile/profile.component';
// import { UserComponent } from './user/user.component';
// import { StoryComponent } from './story/story.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ResumelistComponent } from './resumelist/resumelist.component';
import { AddResumeProfileComponent } from './add-resume-profile/add-resume-profile.component';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';

@NgModule({
  declarations: [SignupComponent, LoginComponent, ResumelistComponent, AddResumeProfileComponent, ProfileDetailsComponent],
  imports: [
    InfiniteScrollModule,
    CommonModule,
    PagesRoutingModule,
    FormsModule,
    ChartsModule,
    HttpClientModule,
    NgSelectModule
  ]
})
export class PagesModule { }
