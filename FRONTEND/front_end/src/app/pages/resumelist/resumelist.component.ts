import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { AppComponent } from 'src/app/app.component';
import { profilelist, User, UserDetails } from 'src/app/core/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resumelist',
  templateUrl: './resumelist.component.html',
  styleUrls: ['./resumelist.component.scss']
})
export class ResumelistComponent implements OnInit {

  public list: profilelist[];
  public u: UserDetails = {
    first_name: '',
    last_name: '',
    is_superuser: false,
    id: 0,
    username: '',
  };

  constructor(private userService: UserService, private appComponent: AppComponent, private router: Router) { }



    ngOnInit() {
        this.appComponent.isLoggedIn = true;
        this.getlist();
        this.getUser();
    }

    getlist(){
      this.userService.getResumeList().subscribe(
        (response) => {
          this.list = response
          console.log(this.list);
        },
        (error) => {
          console.log(error);
        }
      );
    }

    gotoDetails(id: number){
      this.router.navigate([`/profile/`, id]);
    }

    getUser(){
      this.userService.getUser().subscribe(
        (response) => {
          this.u = response;
          console.log(this.u.is_superuser);
        },
        (error) => {
          console.log(error);
        }
      );
    }

}
