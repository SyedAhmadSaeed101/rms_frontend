import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { AppComponent } from 'src/app/app.component';
import { createProfile } from 'src/app/core/models/user';

@Component({
  selector: 'app-add-resume-profile',
  templateUrl: './add-resume-profile.component.html',
  styleUrls: ['./add-resume-profile.component.scss']
})
export class AddResumeProfileComponent implements OnInit {
  public keyword: string;
  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private router: Router, private appComponent: AppComponent) { }

  public profile: createProfile = {
    first_name: "",
    last_name: "",
    dob: null,
    age: 0,
    gender: "",
  };
  private resume: File;


  ngOnInit() {
    this.appComponent.isLoggedIn = true;
  }
  public submit() {
    const data = new FormData();
    this.userService.convertToFormData(data, this.profile);
    data.append('resume', this.resume);

    this.userService.createProfile(data).subscribe(
      (response) => {
        this.router.navigate(['/resumelist']);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  public onfileselected(event: any){
    if(event.target.files.length > 0) 
    {
      this.resume = event.target.files[0]
    }
  }
}
