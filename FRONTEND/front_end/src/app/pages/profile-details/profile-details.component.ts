import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { createProfile } from 'src/app/core/models/user';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit {

  public id: number;
  public profile: createProfile = {
    first_name: '',
    last_name: '',
    dob: null,
    age: 0,
    gender: '',
  }
  public resume: any;

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(() => {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getDetails();
    })
  }

  public getDetails() {
    this.userService.getProfileDetails(this.id).subscribe(
      (response) => {
        this.profile.age = response.age;
        this.profile.dob = response.dob;
        this.profile.first_name = response.first_name;
        this.profile.last_name = response.last_name;
        this.profile.gender = response.gender;
        this.resume = response.resume;
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
