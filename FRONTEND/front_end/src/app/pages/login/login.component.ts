import { Component, OnInit, Input } from '@angular/core';
import { UserLogin } from 'src/app/core/models/user';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private appComponent: AppComponent) { }
  public user: UserLogin = {
    username: '',
    password: '',
  };

  ngOnInit() {
    if(localStorage.getItem('token'))
    {
      this.appComponent.isLoggedIn = true;
      this.router.navigate(['/resumelist']); 
    }
  }

  get emailvalue() {
    return this.user.username;
  }

  public login() {
    this.userService.login(this.user).subscribe(
      (response) => {
        localStorage.setItem('token', response.key);
        console.log(response);
        this.router.navigate(['/resumelist']);
      },
      (error) => {
        alert("wrong username or password");
        console.log(error);
      }
    );
  }
}
