import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddResumeProfileComponent } from './add-resume-profile.component';

describe('AddResumeProfileComponent', () => {
  let component: AddResumeProfileComponent;
  let fixture: ComponentFixture<AddResumeProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddResumeProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddResumeProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
