import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private appComponent: AppComponent) { }

  ngOnInit() {
  }

  logout(){
    this.appComponent.isLoggedIn = false;
    this.userService.logout();
    localStorage.removeItem('token');
    this.router.navigate(['/login'])
  }

}
