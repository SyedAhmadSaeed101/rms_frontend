// import { profile } from './profile';

export interface User {
    is_superuser: boolean;
    first_name: string
    last_name: string;
    username: string;
    id: number;
}

export interface UserDetails {
    is_superuser: boolean;
    first_name: string
    last_name: string;
    username: string;
    id: number;
}

export interface Usersignup {
    username: string
    email: string;
    password1: string;
    password2: string;
}

export interface UserLogin {
    username: string;
    password: string;
}


export interface createProfile{
    first_name: string;
    last_name: string;
    dob: Date;
    age: number;
    gender: string;
}

export interface profilelist{
    pk: number;
    first_name: string;
    last_name: string;
}

