import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private authToken?: string;



  private getAuthToken():string | null {
    return localStorage.getItem('token');
  }

  constructor() {  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
 
    this.authToken = this.getAuthToken();
    if (!this.authToken) {
      return next.handle(request);
    }
    this.authToken = `Token ${this.authToken}`;

    const authorizedRequest = request.clone({
      setHeaders: {
        Authorization: this.authToken,
      },
    });

    return next.handle(authorizedRequest);
  }
}
