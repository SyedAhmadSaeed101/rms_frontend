import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 import { User, UserLogin, UserDetails, Usersignup, createProfile, profilelist } from '../models/user';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
// import { Page } from '../models/page';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public signUp(data: Usersignup): Observable<any> {
    return this.http.post('http://localhost:8000/api/accounts/registration', data);
  }

  public login(data: UserLogin): Observable<any> {
    return this.http.post('http://localhost:8000/api/accounts/login/', data);
  }

  public createProfile(data: any): Observable<any> {
    return this.http.post('http://localhost:8000/api/profile/', data);
  }

  public logout(): Observable<any> {
    return this.http.post('http://localhost:8000/api/accounts/logout/', {});
  }

  public getResumeList(): Observable<Array<profilelist>> {
    return this.http.get<Array<profilelist>>('http://localhost:8000/api/profilelist/').pipe(
      map(response => response)
    );
  }

  public getProfileDetails(id): Observable<any> {
    return this.http.get(`http://localhost:8000/api/profile/${id}`);
  }

  public getUser(): Observable<any> {
    return this.http.get('http://localhost:8000/api/accounts/user/');
  }

  public convertToFormData(formData, data, parentKey?){
    if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)){
      Object.keys(data).forEach(key => {
        this.convertToFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
      })
    } else {
      const value = data == null ? '' : data;
      formData.append(parentKey, value)
    }
  }
}
